using System;
using Xunit;
using Assignment1_Robin_Erik_Hellgren;

namespace Assignment1Tests
{
    public class StandardProgramTests
    {
        [Fact]
        public void Newly_Created_Characters_Level_ReturnLevel1()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            int expected = 1;
            // Act
            int actual = NewChar1.Lvl;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_Character_From_One_To_Two_ReturnLevel2()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            int lhs = 1;
            int expected = 2;
            // Act
            NewChar1.IncreaseLevel(lhs);
            int actual = NewChar1.Lvl;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void Increasing_Level_By_Negative_Or_Zero_ReturnArgumentException(int lhs)
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            // Act and Assert
            Assert.Throws<ArgumentException>(() => NewChar1.IncreaseLevel(lhs));
        }

        [Fact]
        public void Creating_A_Mage_Gives_The_Correct_Attributes_ReturnRightStartingAttributes()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";
            
            int[] expected = { 5, 1, 1, 8 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Mage);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Creating_A_Ranger_Gives_The_Correct_Attributes_ReturnRightStartingAttributes()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";
            
            int[] expected = { 8, 1, 7, 1 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Ranger);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Creating_A_Rogue_Gives_The_Correct_Attributes_ReturnRightStartingAttributes()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";

            int[] expected = { 8, 2, 6, 1 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Rogue);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Creating_A_Warrior_Gives_The_Correct_Attributes_ReturnRightStartingAttributes()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";

            int[] expected = { 10, 5, 2, 1 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Levelling_A_Mage_Will_Give_It_The_Right_Increase_Of_Attributes_ReturnRightAttributesForLevel2Mage()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";

            int[] expected = { 8, 2, 2, 13 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Mage);
            NewChar1.IncreaseLevel(1);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Levelling_A_Ranger_Will_Give_It_The_Right_Increase_Of_Attributes_ReturnRightAttributesForLevel2Ranger()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";

            int[] expected = { 10, 2, 12, 2 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Ranger);
            NewChar1.IncreaseLevel(1);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Levelling_A_Rogue_Will_Give_It_The_Right_Increase_Of_Attributes_ReturnRightAttributesForLevel2Rogue()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";

            int[] expected = { 11, 3, 10, 2 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Rogue);
            NewChar1.IncreaseLevel(1);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Levelling_A_Warrior_Will_Give_It_The_Right_Increase_Of_Attributes_ReturnRightAttributesForLevel2Warrior()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";

            int[] expected = { 15, 8, 4, 2 };
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);
            NewChar1.IncreaseLevel(1);
            int[] actual = NewChar1.basePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Levelling_A_Warrior_Will_Give_It_The_Right_Increase_Of_Secondary_Attributes_ReturnRightSecondaryAttributesForLevel2Warrior()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            string lhs = "Name";

            int[] expected = {150, 12, 2};
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);
            NewChar1.IncreaseLevel(1);
            int[] actual = NewChar1.secondaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void InvalidWeaponException_Should_Be_Thrown_If_Characters_Level_Is_Too_Low_ReturnInvalidWeaponException()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = enum_Slot.Weapon,
                WeaponType = 6,
                WeaponDamage = 7,
                WeaponSpeed = 1.1
            };
            string lhs = "Name";

            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);
            
            // Assert
            Assert.Throws<WeaponException>(() => NewChar1.EquipWeapon(testAxe.ItemName, testAxe.ItemSlot, testAxe.WeaponType, (testAxe.WeaponDamage * testAxe.WeaponSpeed), testAxe.ItemLevel));
        }
        [Fact]
        public void InvalidArmourException_Should_Be_Thrown_If_Characters_Level_Is_Too_Low_ReturnInvalidArmourException()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Armour testPlateBody = new Armour()
            {
            ItemName = "Common plate body armor",
            ItemLevel = 2,
            ItemSlot = enum_Slot.Body,
            ArmourType = 4,
            ArmourAttributes = new int[4] { 2, 1, 0, 0}
            };
        
            string lhs = "Name";

            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);

            // Assert
            Assert.Throws<ArmourException>(() => NewChar1.EquipArmour(testPlateBody.ItemName, testPlateBody.ItemSlot, testPlateBody.ArmourType, testPlateBody.ArmourAttributes, testPlateBody.ItemLevel));
        }
        [Fact]
        public void InvalidWeaponException_Should_Be_Thrown_If_Characters_Try_To_Equip_Weapons_They_Cannot_Use_ReturnInvalidWeaponException()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Weapon,
                WeaponType = 3,
                WeaponDamage = 12, 
                WeaponSpeed = 0.8 
            };
            string lhs = "Name";
;
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);

            // Assert
            Assert.Throws<WeaponException>(() => NewChar1.EquipWeapon(testBow.ItemName, testBow.ItemSlot, testBow.WeaponType, (testBow.WeaponDamage * testBow.WeaponSpeed), testBow.ItemLevel));
        }
        [Fact]
        public void InvalidArmourException_Should_Be_Thrown_If_Characters_Try_To_Equip_Armour_They_Cannot_Use_ReturnInvalidArmourException()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Armour testClothHead = new Armour()
            {
                ItemName = "Common cloth head armour",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Head,
                ArmourType = 1,
                ArmourAttributes = new int[4] { 1, 0, 0, 5 }
            };

            string lhs = "Name";

            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);

            // Assert
            Assert.Throws<ArmourException>(() => NewChar1.EquipArmour(testClothHead.ItemName, testClothHead.ItemSlot, testClothHead.ArmourType, testClothHead.ArmourAttributes, testClothHead.ItemLevel));
        }
        [Fact]
        public void Valid_Weapon_Equipped_And_Shows_A_Message_ReturnMessageOfConfirmation()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Weapon,
                WeaponType = 6,
                WeaponDamage = 7,
                WeaponSpeed = 1.1
            };
            string lhs = "Name";

            string expected = "New Weapon Equipped!";
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);

            // Assert
            Assert.Equal(NewChar1.EquipWeapon(testAxe.ItemName, testAxe.ItemSlot, testAxe.WeaponType, (testAxe.WeaponDamage * testAxe.WeaponSpeed), testAxe.ItemLevel), expected);
        }
        [Fact]
        public void Valid_Armour_Equipped_And_Shows_A_Message_ReturnMessageOfConfirmation()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Armour testPlateBody = new Armour()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Body,
                ArmourType = 4,
                ArmourAttributes = new int[4] { 2, 1, 0, 0 }
            };

            string lhs = "Name";

            string expected = "New Armour Equipped!";
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);

            // Assert
            Assert.Equal(NewChar1.EquipArmour(testPlateBody.ItemName, testPlateBody.ItemSlot, testPlateBody.ArmourType, testPlateBody.ArmourAttributes, testPlateBody.ItemLevel), expected);
        }
        [Fact]
        public void Calculate_DPS_Without_An_Equipped_Weapon_ReturnRightDPS()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            
            string lhs = "Name";

            float expected = 1.05F;
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);
            float actual = (1F + (NewChar1.totalPrimaryAttributes[1] / 100F));
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Calculate_DPS_With_An_Equipped_Weapon_ReturnRightDPS()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Weapon,
                WeaponType = 6,
                WeaponDamage = 7,
                WeaponSpeed = 1.1
            };
            string lhs = "Name";

            float expected = 8.085F;
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);
            NewChar1.EquipWeapon(testAxe.ItemName, testAxe.ItemSlot, testAxe.WeaponType, (testAxe.WeaponDamage * testAxe.WeaponSpeed), testAxe.ItemLevel);
            float actual = Convert.ToSingle((testAxe.WeaponDamage * testAxe.WeaponSpeed) * (1F + (NewChar1.totalPrimaryAttributes[1] / 100F)));
            
            // Assert
            Assert.Equal(actual, expected);
        }
        [Fact]
        public void Calculate_DPS_With_An_Equipped_Weapon_And_Armour_ReturnRightDPS()
        {
            //Arrange
            NewCharacter NewChar1 = new NewCharacter();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Weapon,
                WeaponType = 6,
                WeaponDamage = 7,
                WeaponSpeed = 1.1
            };
            Armour testPlateBody = new Armour()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Body,
                ArmourType = 4,
                ArmourAttributes = new int[4] { 2, 1, 0, 0 }
            };
            string lhs = "Name";

            float expected = 8.162F;
            // Act
            NewChar1.CreateNewChar(lhs, enum_Classes.Warrior);
            NewChar1.EquipWeapon(testAxe.ItemName, testAxe.ItemSlot, testAxe.WeaponType, (testAxe.WeaponDamage * testAxe.WeaponSpeed), testAxe.ItemLevel);
            NewChar1.EquipArmour(testPlateBody.ItemName, testPlateBody.ItemSlot, testPlateBody.ArmourType, testPlateBody.ArmourAttributes, testPlateBody.ItemLevel);
            float actual = Convert.ToSingle((testAxe.WeaponDamage * testAxe.WeaponSpeed) * (1F + (NewChar1.totalPrimaryAttributes[1] / 100F)));

            // Assert
            Assert.Equal(actual, expected);
        }
    }
}

