﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
    /// <summary>
    /// The Armour Class is a child of Item and has the properties of the armours type and its attributes.
    /// </summary>
    public class Armour : Item
    {
        public int ArmourType { get; set; }
        public int[] ArmourAttributes { get; set; } = new int[4];
    }
}
