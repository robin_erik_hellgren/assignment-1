﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
    class Warrior
    {
        /// <summary>
        /// Contains all of the properties for the Warrior class - child of Character
        /// </summary>
        public int[] warriorWeaponEquip { get; } = new int[] { 0, 0, 0, 0, 0, 1, 1, 1 };
        public int[] warriorArmourEquip { get; } = new int[] { 0, 0, 1, 1 };
        public int[] WarriorPrimaryAttributes { get; } = new int[4] { 10, 5, 2, 1 };
        public int[] WarriorPrimaryAttributesIncrease { get; } = new int[4] { 5, 3, 2, 1 };
        public int[] WarriorScaling { get; } = new int[4] { 1, 0, 0, 0 };

    }
}
