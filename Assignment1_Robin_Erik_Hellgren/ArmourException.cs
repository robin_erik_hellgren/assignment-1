﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
   [Serializable]
    public class ArmourException : Exception
    {
        /// <summary>
        /// The ArmourException throws when the character tries to equip an armour that it cannot. Then it returns a string.
        /// </summary>
        public ArmourException() { }

        public ArmourException(string name)
            :base(String.Format("You cannot equip {0}", name))
        {

        }
    }
}
