﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{

    public class Weapon : Item
    {
        /// <summary>
        /// The Weapon Class is a child of Item and has the properties of the weapons type among others.
        /// </summary>
        public int WeaponType { get; set; }
        public double WeaponDamage { get; set; }
        public double WeaponSpeed { get; set; }
    }
}
