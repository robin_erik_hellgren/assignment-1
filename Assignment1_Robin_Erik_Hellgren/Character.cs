﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
    /// <summary>
    /// Enum for the four slots.
    /// </summary>
    public enum enum_Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
    /// <summary>
    /// Enum for the classes that can be chosen
    /// </summary>
    public enum enum_Classes
    {
        Mage,
        Ranger,
        Rogue,
        Warrior
    }
    /// <summary>
    /// Abstract Class Character gives the blueprint for its child NewCharacter and contains most methods and a lot of important properties.
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public string CharacterClass { get; set; }
        public int Lvl { get; protected set; } = 1;
        public int[] basePrimaryAttributes { get; set; }  = new int[4];
        public int[] itemPrimaryAttributes { get; set; } = new int[4] { 0, 0, 0, 0 };
        public int[] totalPrimaryAttributes { get; set; } = new int[4];
        public int[] secondaryAttributes { get; set; } = new int[3];
        public int[] weaponEquip { get; set; } = new int[8];
        public int[] armourEquip { get; set; } = new int[4];
        public int[] Scaling { get; set; } = new int[4];
        public double DPS { get; set; } = 1;

        Dictionary<enum_Slot, string> ItemSlots = new Dictionary<enum_Slot, string>();

        /// <summary>
        /// CreateNewChar takes a name and a class and creates a character. It then sets the primary and secondary attributes
        /// according to the chosen class.
        /// </summary>
        /// <param name="ChosenName"> Is the inserted name will be the name of the character</param>
        /// <param name="ChosenClass"> Is the inserted class that </param>
        public void CreateNewChar(string ChosenName, enum_Classes ChosenClass)
        {
            Name = ChosenName;
            
            if (ChosenClass == enum_Classes.Mage)
            {
                Mage NewMage1 = new Mage();
                basePrimaryAttributes = NewMage1.MagePrimaryAttributes;
                totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                secondaryAttributes[1] = totalPrimaryAttributes[1]+ totalPrimaryAttributes[2];
                secondaryAttributes[2] = totalPrimaryAttributes[3];
                weaponEquip = NewMage1.mageWeaponEquip;
                armourEquip = NewMage1.mageArmourEquip;
                Scaling = NewMage1.MageScaling;
                CharacterClass = "Mage";
            }
            if (ChosenClass == enum_Classes.Ranger)
            {
                Ranger NewRanger1 = new Ranger();
                basePrimaryAttributes = NewRanger1.RangerPrimaryAttributes;
                totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                secondaryAttributes[1] = totalPrimaryAttributes[1] + totalPrimaryAttributes[2];
                secondaryAttributes[2] = totalPrimaryAttributes[3];
                weaponEquip = NewRanger1.rangerWeaponEquip;
                armourEquip = NewRanger1.rangerArmourEquip;
                Scaling = NewRanger1.RangerScaling;
                CharacterClass = "Ranger";
            }
            if (ChosenClass == enum_Classes.Rogue)
            {
                Rogue NewRogue1 = new Rogue();
                basePrimaryAttributes = NewRogue1.RoguePrimaryAttributes;
                totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                secondaryAttributes[1] = totalPrimaryAttributes[1] + totalPrimaryAttributes[2];
                secondaryAttributes[2] = totalPrimaryAttributes[3];
                weaponEquip = NewRogue1.rogueWeaponEquip;
                armourEquip = NewRogue1.rogueArmourEquip;
                Scaling = NewRogue1.RogueScaling;
                CharacterClass = "Rogue";
            }
            if (ChosenClass == enum_Classes.Warrior)
            {
                Warrior NewWarrior1 = new Warrior();
                basePrimaryAttributes = NewWarrior1.WarriorPrimaryAttributes;
                totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                secondaryAttributes[1] = totalPrimaryAttributes[1] + totalPrimaryAttributes[2];
                secondaryAttributes[2] = totalPrimaryAttributes[3];
                weaponEquip = NewWarrior1.warriorWeaponEquip;
                armourEquip = NewWarrior1.warriorArmourEquip;
                Scaling = NewWarrior1.WarriorScaling;
                CharacterClass = "Warrior";
            }


        }
        /// <summary>
        /// The method for increasing levels. First it checks for the exception, then if it does not find
        /// 0 or a negative number it checks what class the character is and then it increases its attributes
        /// accordingly.
        /// </summary>
        /// <param name="LevelsGained"> An int that is inserted to represent the level gain </param>
        public void  IncreaseLevel(int LevelsGained)
             {
                 if(LevelsGained <= 0)
                 {
                     throw new ArgumentException();

                 }
                 else
                 {
                     Lvl += LevelsGained;

                    if (CharacterClass == "Mage")
                    {
                        Mage Mage1 = new Mage();
                        basePrimaryAttributes[0] += Mage1.MagePrimaryAttributesIncrease[0] * (Lvl - 1);
                        basePrimaryAttributes[1] += Mage1.MagePrimaryAttributesIncrease[1] * (Lvl - 1);
                        basePrimaryAttributes[2] += Mage1.MagePrimaryAttributesIncrease[2] * (Lvl - 1);
                        basePrimaryAttributes[3] += Mage1.MagePrimaryAttributesIncrease[3] * (Lvl - 1);
                        totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                        totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                        totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                        totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                        secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                        secondaryAttributes[1] = totalPrimaryAttributes[1] + totalPrimaryAttributes[2];
                        secondaryAttributes[2] = totalPrimaryAttributes[3];
                    }
                    if (CharacterClass == "Ranger")
                    {
                        Ranger Ranger1 = new Ranger();
                        basePrimaryAttributes[0] += Ranger1.RangerPrimaryAttributesIncrease[0] * (Lvl - 1);
                        basePrimaryAttributes[1] += Ranger1.RangerPrimaryAttributesIncrease[1] * (Lvl - 1);
                        basePrimaryAttributes[2] += Ranger1.RangerPrimaryAttributesIncrease[2] * (Lvl - 1);
                        basePrimaryAttributes[3] += Ranger1.RangerPrimaryAttributesIncrease[3] * (Lvl - 1);
                        totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                        totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                        totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                        totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                        secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                        secondaryAttributes[1] = totalPrimaryAttributes[1] + totalPrimaryAttributes[2];
                        secondaryAttributes[2] = totalPrimaryAttributes[3];
                    }
                    if (CharacterClass == "Rogue")
                    {
                        Rogue Rogue1 = new Rogue();
                        basePrimaryAttributes[0] += Rogue1.RoguePrimaryAttributesIncrease[0] * (Lvl - 1);
                        basePrimaryAttributes[1] += Rogue1.RoguePrimaryAttributesIncrease[1] * (Lvl - 1);
                        basePrimaryAttributes[2] += Rogue1.RoguePrimaryAttributesIncrease[2] * (Lvl - 1);
                        basePrimaryAttributes[3] += Rogue1.RoguePrimaryAttributesIncrease[3] * (Lvl - 1);
                        totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                        totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                        totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                        totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                        secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                        secondaryAttributes[1] = totalPrimaryAttributes[1] + totalPrimaryAttributes[2];
                        secondaryAttributes[2] = totalPrimaryAttributes[3];
                    }
                    if (CharacterClass == "Warrior")
                        {
                        Warrior Warrior1 = new Warrior();
                        basePrimaryAttributes[0] += (Warrior1.WarriorPrimaryAttributesIncrease[0] * (Lvl - 1));
                        basePrimaryAttributes[1] += Warrior1.WarriorPrimaryAttributesIncrease[1] * (Lvl - 1);
                        basePrimaryAttributes[2] += Warrior1.WarriorPrimaryAttributesIncrease[2] * (Lvl - 1);
                        basePrimaryAttributes[3] += Warrior1.WarriorPrimaryAttributesIncrease[3] * (Lvl - 1);
                        totalPrimaryAttributes[0] = itemPrimaryAttributes[0] + basePrimaryAttributes[0];
                        totalPrimaryAttributes[1] = itemPrimaryAttributes[1] + basePrimaryAttributes[1];
                        totalPrimaryAttributes[2] = itemPrimaryAttributes[2] + basePrimaryAttributes[2];
                        totalPrimaryAttributes[3] = itemPrimaryAttributes[3] + basePrimaryAttributes[3];
                        secondaryAttributes[0] = totalPrimaryAttributes[0] * 10;
                        secondaryAttributes[1] = totalPrimaryAttributes[1] + totalPrimaryAttributes[2];
                        secondaryAttributes[2] = totalPrimaryAttributes[3];
                    }
                 }
             }

        /// <summary>
        /// The method for equiping a weapon. It checks for the exception to see if the character has the right
        /// level and class to equip it. If it does not it throws to the weapon exception. It then calculates the DPS
        /// Even though I did not end up using that property. Finally it returns a message.
        /// 
        /// </summary>
        /// <param name="WeaponName">This is the name of the equipped weapon</param>
        /// <param name="WeaponSlot">The slot that the weapon uses (weapon)</param>
        /// <param name="WeaponType">The type of weapon (hammer, sword etc)</param>
        /// <param name="WeaponDPS">The weapons calculated DPS</param>
        /// <param name="WeaponLevel">The level required to use the weapon</param>
        /// <returns>Returns a message!</returns>
        public string EquipWeapon(string WeaponName, enum_Slot WeaponSlot, int WeaponType, double WeaponDPS, int WeaponLevel)
        {
            if (WeaponLevel <= Lvl && weaponEquip[(WeaponType-1)] == 1)
            {
                ItemSlots.Add(WeaponSlot, WeaponName);

                if (CharacterClass == "Mage")
                {
                    DPS = WeaponDPS * (1 + totalPrimaryAttributes[3] / 100);
                }
                if (CharacterClass == "Ranger")
                {
                    DPS = WeaponDPS * (1 + totalPrimaryAttributes[2] / 100);
                }
                if (CharacterClass == "Rogue")
                {
                    DPS = WeaponDPS * (1 + totalPrimaryAttributes[2] / 100);
                }
                if (CharacterClass == "Warrior")
                {
                    DPS = WeaponDPS * (1 + totalPrimaryAttributes[1] / 100);
                }
                string message = "New Weapon Equipped!";
                return message;

            }
            else
            {
                throw new WeaponException();
            }
         }
        /// <summary>
        /// The method for equipping armour begins with it checking if the character has the required class and level for the armour.
        /// If it does not meet the requirments it throws to the ArmourException.
        /// Then it adds it to the dictionary and adds up the total of the attributes that it might have contributed to.
        /// </summary>
        /// <param name="ArmourName">The name of the Armour</param>
        /// <param name="ArmourSlot">The Slot that the Armour fits (head, chest etc)</param>
        /// <param name="ArmourType">The type of Armour (mail, cloth etc)</param>
        /// <param name="ArmourAttributes">The attributes that this particular Armour confers</param>
        /// <param name="ArmourLevel">The required level to wear the Armour</param>
        /// <returns>Returns a message!</returns>
        public string EquipArmour(string ArmourName, enum_Slot ArmourSlot, int ArmourType, int[]ArmourAttributes, int ArmourLevel)
        {
            if(ArmourLevel <= Lvl && armourEquip[(ArmourType-1)] == 1)
            {
                ItemSlots.Add(ArmourSlot, ArmourName);
                totalPrimaryAttributes[0] = basePrimaryAttributes[0] + ArmourAttributes[0];
                totalPrimaryAttributes[1] = basePrimaryAttributes[1] + ArmourAttributes[1];
                totalPrimaryAttributes[2] = basePrimaryAttributes[2] + ArmourAttributes[2];
                totalPrimaryAttributes[3] = basePrimaryAttributes[3] + ArmourAttributes[3];
                string message = "New Armour Equipped!";
                return message;
            }
            else
            {
                throw new ArmourException(ArmourName);
            }
        }
    }
}

