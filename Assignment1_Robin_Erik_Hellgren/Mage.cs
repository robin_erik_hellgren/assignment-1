﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
    class Mage : Character
    {
        /// <summary>
        /// Contains all of the properties for the Mage class - child of Character
        /// </summary>
        public int[] mageWeaponEquip { get; } = new int[] { 1, 1, 0, 0, 0, 0, 0, 0 };
        public int[] mageArmourEquip { get; } = new int[] { 1, 0, 0, 0 };
        public int[] MagePrimaryAttributes { get; } = new int[] { 5, 1, 1, 8 };
        public int[] MagePrimaryAttributesIncrease { get; } = new int[] { 3, 1, 1, 5 };
        public int[] MageScaling { get; } = new int[4] { 0, 0, 0, 1 };

    }
}
