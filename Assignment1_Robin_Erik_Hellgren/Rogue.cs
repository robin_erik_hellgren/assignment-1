﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
    class Rogue
    {
        /// <summary>
        /// Contains all of the properties for the Rogue class - child of Character
        /// </summary>
        public int[] rogueWeaponEquip { get; } = new int[] { 0, 0, 0, 1, 1, 0, 0, 0 };
        public int[] rogueArmourEquip { get; } = new int[] { 0, 1, 1, 0 };
        public int[] RoguePrimaryAttributes { get; } = new int[4] { 8, 2, 6, 1 };
        public int[] RoguePrimaryAttributesIncrease { get; } = new int[4] { 3, 1, 4, 1 };
        public int[] RogueScaling { get; } = new int[4] { 0, 0, 1, 0 };

    }
}
