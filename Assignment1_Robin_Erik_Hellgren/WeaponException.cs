﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
   public class WeaponException : Exception
    {
        /// <summary>
        /// The WeaponException throws when the character tries to equip a weapon that it cannot. Then it returns a string.
        /// </summary>
        public WeaponException() { }

        public WeaponException(string name)
            : base(String.Format("You cannot equip {0}", name))
        {

        }
    }
}

