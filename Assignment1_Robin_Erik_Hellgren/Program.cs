﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment1_Robin_Erik_Hellgren
{   
    public class Program
    {
        /// <summary>
        /// Contains the character sheet!
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
          
            string Name = "Tobbe";
            //int LevelIncrease = 1;
            //NewChar1.IncreaseLevel(LevelIncrease);

            NewCharacter NewChar1 = new NewCharacter();
            NewChar1.CreateNewChar(Name, enum_Classes.Mage);

            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Weapon,
                WeaponType = 6,
                WeaponDamage = 7,
                WeaponSpeed = 1.1
            };
            Armour testPlateBody = new Armour()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = enum_Slot.Body,
                ArmourType = 4,
                ArmourAttributes = new int[4] { 2, 1, 0, 0}
            };

            NewChar1.EquipWeapon(testAxe.ItemName,testAxe.ItemSlot, testAxe.WeaponType, (testAxe.WeaponDamage * testAxe.WeaponSpeed), testAxe.ItemLevel);
            NewChar1.EquipArmour(testPlateBody.ItemName, testPlateBody.ItemSlot, testPlateBody.ArmourType, testPlateBody.ArmourAttributes, testPlateBody.ItemLevel);
            float DPS = Convert.ToSingle((testAxe.WeaponDamage * testAxe.WeaponSpeed) * (1F + (NewChar1.totalPrimaryAttributes[1] / 100F)));

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Character Name: {NewChar1.Name}");
            sb.AppendLine($"Current Level: {NewChar1.Lvl}");
            sb.AppendLine($"Strenght: {NewChar1.totalPrimaryAttributes[1]} ");
            sb.AppendLine($"Dexterity: {NewChar1.totalPrimaryAttributes[2]} ");
            sb.AppendLine($"Intelligence: {NewChar1.totalPrimaryAttributes[3]} ");
            sb.AppendLine($"Health: {NewChar1.secondaryAttributes[0]}");
            sb.AppendLine($"Armor Rating: {NewChar1.secondaryAttributes[1]}");
            sb.AppendLine($"Elemental Resistance: {NewChar1.secondaryAttributes[2]}");
            sb.AppendLine($"DPS: {DPS}");
            Console.WriteLine(sb);

        }
    }
}