﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
    public abstract class Item
    {
        /// <summary>
        /// The Parent Class of Armour and Weapon, confers a few properties.
        /// </summary>
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public enum_Slot ItemSlot { get; set; }
    }
}
