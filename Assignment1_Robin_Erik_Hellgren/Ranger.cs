﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_Robin_Erik_Hellgren
{
    class Ranger
    {
        /// <summary>
        /// Contains all of the properties for the Ranger class - child of Character
        /// </summary>
        public int[] rangerWeaponEquip { get; } = new int[] { 0, 0, 1, 0, 0, 0, 0, 0 };
        public int[] rangerArmourEquip { get; } = new int[] { 0, 1, 1, 0 };
        public int[] RangerPrimaryAttributes { get; } = new int[4] { 8, 1, 7, 1 };
        public int[] RangerPrimaryAttributesIncrease { get; } = new int[4] { 2, 1, 5, 1 };
        public int[] RangerScaling { get; } = new int[4] { 0, 0, 1, 0 };

    }
}

